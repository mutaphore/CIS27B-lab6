
public class Card {

	// private data
   private char value;
   private int suit;
   
   // static class constants (for suits)
   public static final int clubs = 0;
   public static final int diamonds = 1;
   public static final int hearts = 2;
   public static final int spades = 3;
   
   // 4 overloaded constructors
   public Card(char value, int suit)
   {
      Set(value, suit);
   }
   public Card(char value)
   {
      this(value, spades);
   }
   public Card()
   {
      this('A', spades);
   }
   // copy constructor
   public Card(Card card)
   {
      this.suit = card.suit;
      this.value = card.value;
   }

   // mutator
   public boolean Set(char value, int suit)
   {
      char up_val;            // for upcasing char
      boolean valid = true;   // return value
      
      // filter out bad suit input:

      if (suit == clubs || suit == diamonds 
            || suit == hearts || suit == spades)
         this.suit = suit;
      else
      {
         valid = false;
         this.suit = spades;
      }
   
      // convert to uppercase to simplify
      up_val = Character.toUpperCase(value);
      // check for validity
      if (
         up_val == 'A' || up_val == 'K'
         || up_val == 'Q' || up_val == 'J'
         || up_val == 'T'
         || (up_val >= '2' && up_val <= '9')
         )
         this.value = up_val;
      else
      {
         valid = false;
         this.value = 'A';
      }
      return valid;
   }
   
   // accessors
   public char GetVal()
   {
      return value;
   }
   public int GetSuit()
   {
      return suit;
   }
      
   // stringizer
   public String toString()
   {
      String ret_val;

      // convert from char to String
      ret_val =  String.valueOf(value);

      if (suit == spades)
         ret_val += " of Spades";
      else if (suit == hearts)
         ret_val += " of Hearts";
      else if (suit == diamonds)
         ret_val += " of Diamonds";
      else if (suit == clubs)
         ret_val += " of Clubs";

      return ret_val;
   }
   
}
