
public class CardQueue extends Queue{
	
	//Constructor--------------------------
	public CardQueue(){
		
		super();
		
	}
	
	//Mutators--------------------------
	public void AddCard(Card card) {
		
		CardNode cn = new CardNode(card);
		super.Add(cn);
		
	}
	
	public void AddCard(char value, int suit) {
		
		CardNode cn = new CardNode(value,suit);
		super.Add(cn);
		
	}
	
	public CardNode RemoveCard() {
		
		return (CardNode)super.Remove();
		
	}
	
	//Helpers--------------------------
	public void ShowQueue() {
		
		CardNode walker;
		
		walker = (CardNode)super.head;
		
		if(super.head == null && super.tail == null) 
			System.out.println("\nNo Card in queue!");
		else {
			
			System.out.print("\nShow Card Queue: ");
			
			do {
			
				walker.Show();
				walker = (CardNode)walker.next;
				
			}while(walker != null);
		
		}
		
	}
	
}
