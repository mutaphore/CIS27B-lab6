
public class Foothill {

	public static void main(String[] args) {
		
		//Test the Generic Nodes
		Queue myq = new Queue();
		Node n1 = new Node();
		Node n2 = new Node();
		Node n3 = new Node();

		myq.Add(n1);
		myq.Add(n2);
		myq.Add(n3);
		
		myq.ShowQueue();
		
		for(int i = 0; i < 3; i++) {
			
			myq.Remove();
			myq.ShowQueue();
			
		}
		
		//Test the derived classes CardNode and CardQueue
		
		CardQueue cardq = new CardQueue();
		Card c1 = new Card('4',Card.clubs);
		Card c2 = new Card('A',Card.hearts);
		
		cardq.AddCard(c1);
		cardq.AddCard(c2);
		cardq.AddCard('J',Card.diamonds);
		cardq.AddCard('K',Card.spades);
		
		cardq.ShowQueue();
		
		for(int i = 0; i < 4; i++) {
			
			cardq.RemoveCard();
			cardq.ShowQueue();
			
		}
		
	}

}
