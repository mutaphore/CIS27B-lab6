
public class Queue {

	protected Node head;
	protected Node tail;
	
	public Queue() {
		
		head = null;
		tail = null;
		
	}
	
	//Mutators--------------------------
	public void Add(Node new_node) {
		
		Node temp;
		
		if(head == null && tail == null) {
			
			head = new_node;
			tail = new_node;
		
		}
		else {
		
			temp = tail;
			tail = new_node;
			temp.next = tail;
			tail.next = null;
		
		}
			
	}
	
	public Node Remove() {
		
		Node temp;
		Node returnNode;
		
		returnNode = head;
		
		if(head != null && tail != null) {
		
			if(head == tail) {
			
				head = null;
				tail = null;
		
			}
			else {
				
				temp = head;
				head = temp.next;
				temp.next = null;
				temp = null;
			
			}
		
		}
		
		return returnNode;
			
	}
	
	public void ShowQueue() {
		
		Node walker;
		
		walker = head;
		
		if(head == null && tail == null) 
			System.out.println("\nNothing in queue!");
		else {
			
			System.out.print("\nShow Queue: ");
			
			do {
			
				walker.Show();
				walker = walker.next;
				
			}while(walker != null);
		
		}
	}
	
}
