
public class CardNode extends Node{
	
	private Card card;
	
	//Constructors------------------------------------
	public CardNode() {
		
		super();
		this.card = new Card();
		
	}
	
	public CardNode(char value, int suit) {
		
		super();
		this.card = new Card(value, suit);
		
	}
	
	//Copy Constructor
	public CardNode(Card card) {
		
		super();
		this.card = new Card(card);
		
	}
	
	//Accessors------------------------------------
	public Card GetCard() {
		
		return card;
		
	}
	
	//Helpers------------------------------------
	public void Show(){
		
		System.out.print(" < " + card + " > ");
		
	}

}
